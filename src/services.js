import axios from 'axios';
import React from 'react';
export function getProdotti(){
    axios.get( process.env.CMS+'folder_prodotti/prodotti/lista-prodotti.xml')
    .then( response => {
        // console.log(response);
        return response.data;
    } );

}
