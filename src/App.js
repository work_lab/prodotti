import React from 'react';
import services from './services'
import axios from 'axios';
import { Grid,Row,Col,FormGroup,Radio, ListGroup, ListGroupItem, Button } from 'react-bootstrap';


  
class App extends React.Component {
    
    constructor(props,context){
        console.log(process.env);
        console.log(process.env.CMS_URL);
        
        super(props);
        this.state = {
            filtroPrezzo: '',
            filtroTipologia:''
        };
        this.handleFiltroPrezzoChange = this.handleFiltroPrezzoChange.bind(this);
        this.handleFiltroTipologiaChange = this.handleFiltroTipologiaChange.bind(this);
    } 
    handleFiltroPrezzoChange(filtroPrezzo) {
        this.setState({
            filtroPrezzo: filtroPrezzo
        });
      }
      handleFiltroTipologiaChange(filtroTipologia) {
        this.setState({
            filtroTipologia: filtroTipologia
        });
      }
    render() {
        //this.chiamaServizio();
        
        return (<Grid>
                <Row className="show-grid">
                   
                    <SezioneFiltri  filtroPrezzo={this.state.filtroPrezzo} onFiltroPrezzoChange={this.handleFiltroPrezzoChange} filtroTipologia={this.state.filtroTipologia} onFiltroTipologiaChange={this.handleFiltroTipologiaChange}></SezioneFiltri>
                    <ProdottiLista filtroPrezzo={this.state.filtroPrezzo} filtroTipologia={this.state.filtroTipologia}></ProdottiLista>
                </Row>
            
        </Grid>);
    }
  }
  class SezioneFiltri extends React.Component{
     rangePrice=[false,false,false,false];
    constructor(props){

        super(props);
        console.log(this.props);
        this.handleFiltroPrezzoChange = this.handleFiltroPrezzoChange.bind(this);
        this.handleFiltroTipologiaChange = this.handleFiltroTipologiaChange.bind(this);
      }
      handleFiltroPrezzoChange(e) {
        console.log(e);
        this.rangePrice=[false,false,false,false];
        if (e.target.id!=4) {
            this.rangePrice[e.target.id]=e.target.checked;
            
        }else{
            this.rangePrice="";
        }
        
        console.log(this.rangePrice);
        this.props.onFiltroPrezzoChange(this.rangePrice);
      }
      handleFiltroTipologiaChange(e) {
        console.log(e.target.id);
    
        this.props.onFiltroTipologiaChange(e.target.id);
      }
      render(){
          return(
            <Col xs={6} md={3}>
                <div className="box-filtro">

                <div className="cont-interno">
                        <div >
                            <h4>Prezzo</h4>
                            <div >
                              
                                    
                                    

                                    <FormGroup>
                                    <ListGroup>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup" inline onChange={this.handleFiltroPrezzoChange} id='0'><span>  meno di 150</span></Radio> 
                                        </ListGroupItem>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup" inline onChange={this.handleFiltroPrezzoChange} id='1'>  150 - 300</Radio> 
                                        </ListGroupItem>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup" inline onChange={this.handleFiltroPrezzoChange} id='2'>  300 - 400</Radio> 
                                        </ListGroupItem>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup" inline onChange={this.handleFiltroPrezzoChange} id='3'>  più di 400</Radio> 
                                        </ListGroupItem>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup" inline onChange={this.handleFiltroPrezzoChange} id='4'>  tutti</Radio> 
                                        </ListGroupItem>
                                    </ListGroup>
                                    
                                    </FormGroup>
                                    
                                    
                                    
                                </div>
                            </div>
                            <div >
                            <h4>Tipologia</h4>
                            <div >
                              
                                    
                                    

                                    <FormGroup>
                                    <ListGroup>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup1" inline onChange={this.handleFiltroTipologiaChange} id='smartphone'><span>smartphone</span></Radio> 
                                        </ListGroupItem>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup1" inline onChange={this.handleFiltroTipologiaChange} id='fisso'>fisso</Radio> 
                                        </ListGroupItem>
                                        <ListGroupItem>    
                                            <Radio name="radioGroup1" inline onChange={this.handleFiltroTipologiaChange} id='tutti'>tutti</Radio> 
                                        </ListGroupItem>
                                        <ListGroupItem>    
                                        </ListGroupItem>
                                    </ListGroup>
                                    
                                    </FormGroup>
                                    
                                    
                                    
                                </div>
                            </div>
                            
                    </div>

                </div>
            </Col>
          );
      }
  }
  class ProdottiLista extends React.Component{
    
    state = {
        prodotti: []
    }
      constructor(props){
        super(props);
        const filtroPrezzo = this.props.filtroPrezzo;
        const filtroTipologia = this.props.filtroTipologia;
        console.log("filtroPrezzo"+filtroPrezzo);
      }
      componentDidMount(){
        
        axios
        .get('http://40.68.91.109:4503/content/we-retail/api/faqs.model.json')
        .then(
            response=>{
                console.log(response);
               
            }
        )
          axios
            .get(process.env.REACT_APP_CMS_URL+'/cockpit-master/api/collections/get/prodotto?token=193f5bf31f4d33b0b06f76d90b462c')
            .then(
                response=>{
                    console.log(response.data.entries);
                    this.setState({prodotti: response.data.entries});
                }
            )
      }

      render(){
        const filtroPrezzo = this.props.filtroPrezzo;
        const filtroTipologia = this.props.filtroTipologia;
        console.log("filtroPrezzo"+filtroPrezzo);
        console.log("filtroT"+filtroTipologia);
         const prodottoFinito= this.state.prodotti
         .filter(prodotto=>{
            if (filtroTipologia == "" || filtroTipologia == "tutti" || filtroTipologia==prodotto.tipologia) {
                   return prodotto
            }
         })
         .filter(prodotto=>{
             
             if (filtroPrezzo != "") {
                 console.log(prodotto.prezzoUs)
                switch (true) {
                    case filtroPrezzo[0]:
                    console.log('selezionato <150')
                    console.log(parseInt(prodotto.prezzoUs))
                    if (parseInt(prodotto.prezzoUs)<150) {
                            return prodotto;
                        } 
                    break;
                    case filtroPrezzo[1]:
                        if (parseInt(prodotto.prezzoUs)>150 && parseInt(prodotto.prezzoUs)<300) {
                            return prodotto;
                        } 
                    break;
                    case filtroPrezzo[2]:
                    if (parseInt(prodotto.prezzoUs)>300 && parseInt(prodotto.prezzoUs)<=400) {
                        return prodotto;
                    } 
                    case filtroPrezzo[3]:
                    if (parseInt(prodotto.prezzoUs)>400) {
                        console.log('>400')
                        return prodotto;
                    } 
                    break;
                    default:
                        break;
                }
             }else{
                 return prodotto;
             }
         })
         .map(prodotto=>{
            return(<Prodotto prod={prodotto} key={prodotto._id}></Prodotto>)  
            
        });
          return(
            <Col xs={12} md={9}>
                <Row className="show-grid">
                    {prodottoFinito}
                </Row>
            </Col>
          );
      }
  }
  class Prodotto extends React.Component{
      constructor(props){
          super(props);
         
      }
     /*handleClick(e) {
        e.preventDefault();
        console.log('The link was clicked.');
        const event = new CustomEvent('idDettaglio', { detail: 1 });
        window.dispatchEvent(event);
        console.log(event);
        window.location="http://localhost:8081/dettaglio";
      }*/
      render(){
        console.log(this.props.prod.prezzoRateale);
        var url=process.env.REACT_APP_CMS_URL+""+this.props.prod.img.path;
          return(
            <Col xs={6} md={4} >
            <div className="cont-carousel">
            
           <div className="listingImgContainer">
              <img src={url}/>
           </div>
           <h5>
                {this.props.prod.nome} 
           </h5>
           <div className="info-prezzi">
            {this.props.prod.prezzoRateale.totale ? (
                    <span className="text-warning text-danger text-red">{this.props.prod.prezzoRateale.rataMese} €/4 sett.</span>
                    
                   
            ):(
                <span className="text-warning text-danger text-red">{this.props.prod.prezzoUs} €</span>        
            )
            }
            {this.props.prod.prezzoRateale.totale  &&
                <p>in {this.props.prod.prezzoRateale.numRate} rate a interessi zero<br/><span className="small">se hai una linea di casa TIM</span></p>
            }
            <br  />

           </div>
          
           <div className="footer-btn footerDettagli"  >
           
              <a href={'http://40.115.56.240:8081/dettaglio?id='+this.props.prod._id} className="linkButton" >dettagli</a>
              <Button onClick={addToCart( this.props.prod._id)}   bsStyle="link" className="ico_carrello nobordo">aggiungi</Button>
           </div>
        </div>
        </Col>
          );
      }
  }
  const addToCart = (item) => () => {
    
    const event = new CustomEvent('addToCart', { detail: item });
    window.dispatchEvent(event);
  }
  export default App;